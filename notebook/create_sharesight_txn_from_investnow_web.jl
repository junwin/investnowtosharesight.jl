### A Pluto.jl notebook ###
# v0.19.47

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ d5991305-7cb5-40a0-b835-1c056f56cdf4
begin
    import Pkg
	display(Base.current_project())
    Pkg.activate(Base.current_project())
	#Pkg.add("..")
    #Pkg.activate("..")
	Pkg.instantiate()
	#display(Pkg.status())
    using Revise, HTTP, JSON, JSONTables, DataFrames, Dates, PlutoUI, CSV, Chain
	using InvestNow, Sharesight, InvestNowToSharesight
end

# ╔═╡ c2002c98-bf9a-4a59-9473-922bdb6bfcec
begin
	include("../config/secrets.jl")
	
	investnow_credentials = InvestNowCredentials(username, password, managerId)
	
	md"InvestNow: Using username $(investnow_credentials.username) with managerId of $(investnow_credentials.managerId)"
end

# ╔═╡ abfc9a3a-cd48-11ed-3d29-bfe08bfefdac
md"""
# Create trades on Sharesight for new InvestNow trades
"""

# ╔═╡ d473114e-c9bb-4170-8862-1041b31f5e02
md"""
Run the Pluto notebook using the command:

```sh
$ julia --project -e 'using Pluto; Pluto.run();'
```

Or in the REPL:

```jl
julia> using Pluto; Pluto.run()
```

"""

# ╔═╡ 4022d332-a200-4c97-808a-bede4931979a
@bind triggerInvestNowPasscode LabelButton("Click to have a new InvestNow passcode sent via SMS")

# ╔═╡ e897f1fd-8a84-4af9-b1ee-5d2cdbebb2d4
begin
	triggerInvestNowPasscode
	
	#display("Button was clicked.")
	#display("triggerInvestNowPasscode is: $triggerInvestNowPasscode")
	
	triggerpasscode(investnow_credentials)
	md"Passcode was requested"
end

# ╔═╡ 158911da-9bde-4e82-92d5-b12d8066e266
md"Passcode: $(@bind passcode confirm(TextField()))"

# ╔═╡ 7deef5e4-4a8d-43d8-8b4b-fc944756bd05
begin
	passcode 
	if passcode > ""
		oauthinfo = login(investnow_credentials, passcode)
	end
end

# ╔═╡ 7e95c656-3911-4f3d-9881-1b1f4d2a6e07
begin
	in_portfolios_df, oauthinfo2 = getportfolios(oauthinfo, investnow_credentials)
	#oauthinfo1, portfolios = Sharesight.get_portfolios(oauthinfo, credentials)
	display("$(nrow(in_portfolios_df)) portfolios")
	#display(keys(portfolios[1]))
	for p in eachrow(in_portfolios_df)
		display("$(p["entityName"]), id:$(p["portfolioId"])")
	end
	#display(oauthinfo1)
end

# ╔═╡ 268d40db-625f-40ff-bf75-ff4b4d30961b
begin
	in_portfolios = [ in_portfolios_df[!, "portfolio"][i] => in_portfolios_df[!, "entityName"][i] for i = 1:nrow(in_portfolios_df) ]
	#display(in_portfolios)
	md""
end

# ╔═╡ 20e71b8c-6699-4e0f-9831-29c034595505
md"Select a portfolio: $(@bind selected_investnow_portfolio Select(in_portfolios))"

# ╔═╡ b7f44257-c042-4b76-989c-d8cae8e35819
md"Portfolio selected is: $(selected_investnow_portfolio.portfolioId), $(selected_investnow_portfolio.entityName)"

# ╔═╡ 3597d5c9-937c-4cd8-a6dc-7a5e056ed6bb
begin

	today = Dates.format(Dates.now(), "YYYY-mm-dd")
	lastMonthEnd = Date(Dates.lastdayofmonth(Dates.now() - Dates.Month(1)))
	thisMonthEnd = Date(Dates.lastdayofmonth(Dates.now()))
	nextMonthEnd = Date(Dates.lastdayofmonth(Dates.now() + Dates.Month(1)))
	
	println("lastMonthEnd: $lastMonthEnd\nthisMonthEnd: $thisMonthEnd\nnextMonthEnd: $nextMonthEnd")
	
	periodType = InvestNow.YEAR
	md""
end

# ╔═╡ 7be8eb80-358b-4d28-a0eb-19fade0b69ae
begin
	in_txn_df, oauthinfo3 = getinvestmentactivity(selected_investnow_portfolio, thisMonthEnd, periodType, oauthinfo2, investnow_credentials)

	sort!(in_txn_df)
	
	in_txn_df
end

# ╔═╡ 0b27ee59-5d77-4f57-8828-6cc760f31e46
md"There are $(nrow(in_txn_df)) transactions"

# ╔═╡ d883df35-3a44-4de4-a74a-f47ba4075db7
# Create a new column containing `SharesightTransaction`s derived from the `InvestNowTransaction`s
begin
	in_txn_with_ss_conversion_df = in_txn_df
	in_txn_with_ss_conversion_df.ss_txn .= SharesightTransaction.(in_txn_with_ss_conversion_df.transaction)
	
	in_txn_with_ss_conversion_buysell_df = filter(:ss_txn => ss_txn -> Sharesight.buysellfilter(ss_txn), in_txn_with_ss_conversion_df)

	nothing
end

# ╔═╡ 0a0d64e1-cfc2-40b1-bdf1-e55f77ac1a49
md"There are $(nrow(in_txn_with_ss_conversion_buysell_df)) Buy & Sell transactions"

# ╔═╡ 0066d1a6-b7f5-493a-8373-20ced9e1f153
begin
	if nrow(in_txn_with_ss_conversion_df) > 0
		in_summary_df = combine(in_txn_with_ss_conversion_df, nrow, :date => minimum, :date => maximum)
		start_date = Date(in_summary_df[!, 2][1][1:10], "y-m-d")
		end_date = Date(in_summary_df[!, 3][1][1:10], "y-m-d")
	else
		start_date = Dates.today()
		end_date = Dates.today()
	end	
	#display("Transactions span from $start_date to $end_date")
	nothing
end

# ╔═╡ da626877-a22b-4a34-9f96-c727a478c78e
md"Transactions span from $start_date to $end_date"

# ╔═╡ b6a3ccc8-c508-4edb-962b-2662119c6bd6
md"# Get Sharesight transactions"

# ╔═╡ 26cda76f-b226-4a11-a112-536a230156a8
begin
	ss_credentials = SharesightClientCredentials(client_id, client_secret)
	ss_oauthinfo = Sharesight.refresh_oauth(nothing, ss_credentials)
	#display(oauthinfo)
	nothing
end

# ╔═╡ 5a1443ed-9d83-4718-9473-9cbd7c44ad4b
# Get portfolios
begin
	ss_oauthinfo1, ss_portfolios = Sharesight.get_portfolios(ss_oauthinfo, ss_credentials)
	#display("$(length(ss_portfolios)) portfolios")
	#display(keys(portfolios[1]))
	for p in ss_portfolios
		#display("$(p["name"]), id:$(p["id"])")
	end
	#display(oauthinfo1)
	ss_portfolio_ids = [ i => ss_portfolios[i]["name"] for i in eachindex(ss_portfolios)]
	nothing
end


# ╔═╡ de14bac6-a538-46aa-8baf-79fdcbc133ac
md"Select the Sharesight portfolio: $(@bind selected_ss_portfolio Select(ss_portfolio_ids))"

# ╔═╡ 6ebe8fe0-bffc-4615-98d4-c07b0ff685c4
md"""Requesting portfolio: $(ss_portfolio_ids[selected_ss_portfolio][2])  [ $(selected_ss_portfolio) ] \
Dates from: $start_date to $end_date"""

# ╔═╡ faffa8fd-8126-4814-b53b-c4f43744cf10
begin
	ss_oauthinfo2, ss_txn_df = Sharesight.get_trades_df(ss_oauthinfo, ss_credentials, ss_portfolios[selected_ss_portfolio]["id"], start_date=start_date, end_date=end_date)
	ss_txn_df
end

# ╔═╡ 46e76667-50e6-43f8-bdfe-07dc7a42f142
CSV.write("../output/ss_txn_df.csv", ss_txn_df; transform=(col, val) -> something(val, missing))

# ╔═╡ a1825c2f-cf14-4bd2-8ce7-bf77a09d7489
InvestNowToSharesight.writetxncsv(ss_txn_df[!, :transaction], "../output/ss_txn_df_Sharesighttransaction.csv")

# ╔═╡ e6664450-d5d8-4d5b-89ac-5f7267105f21
md"There are $(nrow(ss_txn_df)) Sharesight transactions in total for the date range $start_date to $end_date"

# ╔═╡ 939dc634-036c-4a2a-a4c9-53443196b12a
md"Transactions from Investnow that have not been recorded in Sharesight:"

# ╔═╡ 5a8510b2-cf70-4bbd-8fef-c520987aeaca
begin
	unrecorded_trades_df_buysell = DataFrame(filter(Sharesight.buysellfilter, antijoin(in_txn_df, ss_txn_df, on=[:ss_txn => :transaction], makeunique=true).ss_txn))
	unrecorded_trades_df_buysell[!, [:tradedate, :quantity, :price, :amount, :comment]]
end

# ╔═╡ d4aa0281-6f41-4c4e-90af-0076c5484899
begin
	start_date_serial = datetime2unix(DateTime(start_date))
	end_date_serial = datetime2unix(DateTime(end_date))
	nothing
end

# ╔═╡ 5affce0d-2d6c-4d44-bed7-0064404b96f2
md"""
Start: $(@bind daterangestart Slider(start_date_serial:end_date_serial))
End: $(@bind daterangeend Slider(start_date_serial:end_date_serial, default=end_date_serial))
"""

# ╔═╡ 43f75dfe-55b1-48c1-a1a7-fa15c6029fe9
begin
	#daterangestart
	new_start_date = Date(unix2datetime(daterangestart))
	new_end_date = Date(unix2datetime(daterangeend))
	nothing
end

# ╔═╡ 480c650f-e4d9-4b15-a619-52177b14592f
md"Filtering dates from $new_start_date to $new_end_date"

# ╔═╡ 72a5a2e0-c350-4645-b785-a9c2e96d1ad5
begin
	filtered_unrecorded_trades_df = filter(:tradedate => tradedate -> tradedate >= new_start_date && tradedate <= new_end_date, unrecorded_trades_df_buysell)
	
	nothing
	#select(filtered_unrecorded_trades_df2, [:tradedate, :comment])
end

# ╔═╡ e37434cd-7658-4e27-81aa-dc8199e39ff3
begin
	# descriptions = Dates.format.(filtered_unrecorded_trades_df.tradedate, dateformat"yyyy-mm-dd ") .* filtered_unrecorded_trades_df.comment
	descriptions = Dates.format.(filtered_unrecorded_trades_df[!, :tradedate], dateformat"yyyy-mm-dd ") .* filtered_unrecorded_trades_df[!, :comment]
	
	#rows2 = 1:length(descriptions2)
	#rows_descriptions_map2 = [eachrow(rows2) => eachrow(descriptions2)]
	description_pairs = collect(pairs(descriptions))

	nothing
end

# ╔═╡ 83434c11-766b-4d52-b66f-437a4e71af35
md"Click transactions to send to Sharesight:"

# ╔═╡ fd1842d6-b10b-46bb-bd3b-7bfcf80a9256
@bind selected_trades_index MultiCheckBox(description_pairs)

# ╔═╡ 649a9454-6156-4900-b898-52ceb897ad42
begin
	selected_trades_df = filtered_unrecorded_trades_df[selected_trades_index, :]
	nothing
end

# ╔═╡ 13ecb12c-3d2d-427c-8bbd-cb2750f84e10
md"The following transactions will be sent to Sharesight:"

# ╔═╡ 31a17748-1376-4d34-9d91-7ed1a502df4f
md"$(selected_trades_df[!, [:tradedate, :quantity, :price, :amount, :comment]])"

# ╔═╡ 92a0a113-497e-45ac-98f3-e1cd65cf254d
md"Check this box to post to Sharesight: $(@bind unlock_for_posting CheckBox())"

# ╔═╡ 3c60f367-2688-4b96-8f65-8ce0b471a240
let
	ss_oauthinfo3 = ss_oauthinfo2
	
	if unlock_for_posting
		for trade in eachrow(selected_trades_df)
			display(trade)

			ss_oauthinfo3, resp_body = Sharesight.create_trade(ss_oauthinfo3, ss_credentials, ss_portfolios[selected_ss_portfolio]["id"], trade.tradedate, trade.transactiontype, trade.marketcode, trade.instrumentcode, trade.comment, trade.quantity, trade.price, trade.amount, trade.currency, trade.uniqueId)
		end
	end

end

# ╔═╡ Cell order:
# ╟─abfc9a3a-cd48-11ed-3d29-bfe08bfefdac
# ╟─d473114e-c9bb-4170-8862-1041b31f5e02
# ╟─d5991305-7cb5-40a0-b835-1c056f56cdf4
# ╟─c2002c98-bf9a-4a59-9473-922bdb6bfcec
# ╟─4022d332-a200-4c97-808a-bede4931979a
# ╟─e897f1fd-8a84-4af9-b1ee-5d2cdbebb2d4
# ╠═158911da-9bde-4e82-92d5-b12d8066e266
# ╟─7deef5e4-4a8d-43d8-8b4b-fc944756bd05
# ╟─7e95c656-3911-4f3d-9881-1b1f4d2a6e07
# ╟─268d40db-625f-40ff-bf75-ff4b4d30961b
# ╟─20e71b8c-6699-4e0f-9831-29c034595505
# ╟─b7f44257-c042-4b76-989c-d8cae8e35819
# ╟─3597d5c9-937c-4cd8-a6dc-7a5e056ed6bb
# ╟─7be8eb80-358b-4d28-a0eb-19fade0b69ae
# ╟─0b27ee59-5d77-4f57-8828-6cc760f31e46
# ╟─0a0d64e1-cfc2-40b1-bdf1-e55f77ac1a49
# ╟─d883df35-3a44-4de4-a74a-f47ba4075db7
# ╟─0066d1a6-b7f5-493a-8373-20ced9e1f153
# ╟─da626877-a22b-4a34-9f96-c727a478c78e
# ╟─b6a3ccc8-c508-4edb-962b-2662119c6bd6
# ╟─26cda76f-b226-4a11-a112-536a230156a8
# ╟─5a1443ed-9d83-4718-9473-9cbd7c44ad4b
# ╟─de14bac6-a538-46aa-8baf-79fdcbc133ac
# ╟─6ebe8fe0-bffc-4615-98d4-c07b0ff685c4
# ╟─faffa8fd-8126-4814-b53b-c4f43744cf10
# ╟─46e76667-50e6-43f8-bdfe-07dc7a42f142
# ╟─a1825c2f-cf14-4bd2-8ce7-bf77a09d7489
# ╟─e6664450-d5d8-4d5b-89ac-5f7267105f21
# ╟─939dc634-036c-4a2a-a4c9-53443196b12a
# ╟─5a8510b2-cf70-4bbd-8fef-c520987aeaca
# ╟─d4aa0281-6f41-4c4e-90af-0076c5484899
# ╟─5affce0d-2d6c-4d44-bed7-0064404b96f2
# ╟─43f75dfe-55b1-48c1-a1a7-fa15c6029fe9
# ╟─480c650f-e4d9-4b15-a619-52177b14592f
# ╟─72a5a2e0-c350-4645-b785-a9c2e96d1ad5
# ╟─e37434cd-7658-4e27-81aa-dc8199e39ff3
# ╟─83434c11-766b-4d52-b66f-437a4e71af35
# ╟─fd1842d6-b10b-46bb-bd3b-7bfcf80a9256
# ╟─649a9454-6156-4900-b898-52ceb897ad42
# ╟─13ecb12c-3d2d-427c-8bbd-cb2750f84e10
# ╟─31a17748-1376-4d34-9d91-7ed1a502df4f
# ╟─92a0a113-497e-45ac-98f3-e1cd65cf254d
# ╟─3c60f367-2688-4b96-8f65-8ce0b471a240
