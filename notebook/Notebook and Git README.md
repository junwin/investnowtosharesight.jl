# Stripping notebook output before committing to git

## `nbconvert`

Install nbconvert. I used the Python package `pip` (on Ubuntu, with python 3 and associated pip intalled).

```sh
pip install nbconvert
```

## Git

Information obtained from here: [How to commit jupyter notebooks without output to git while keeping the notebooks outputs intact locally](https://gist.github.com/33eyes/431e3d432f73371509d176d0dfb95b6e#file-commit_jupyter_notebooks_code_to_git_and_keep_output_locally-md).

Output cells in a notebook typically change every time the notebook is run. That created issues with git because git recognises a change.

The output may also contain sensitive information -- that is not appropriate for a public git repository.

To strip output cells from any commits to git:

1. Run the following (adds to the local `.git/config` file):

```sh
git config filter.strip-notebook-output.clean 'jupyter nbconvert --ClearOutputPreprocessor.enabled=True --to=notebook --stdin --stdout --log-level=ERROR'  
```

2. Add this to `.gitattributes` file in the repository:

```
*.ipynb filter=strip-notebook-output  
```
