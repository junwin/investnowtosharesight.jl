#import InvestNowExporter

#include("../src/InvestNowExporter.jl")

using InvestNowToSharesight
using InvestNow
using Sharesight
using Test
using Dates
using DataFrames

@testset "Check testing works" begin
    @test 1 == 2 - 1
end

@testset "Lookup Sharesight details" begin
    @test InvestNowToSharesight.lookupsharesight("Vanguard International Shares Select Exclusions Index Fund") == (sharesightfund = "Vanguard International Shares Select Exclusions Index Fund", instrumentcode = "VAN1579AU", marketcode = "FundAU")
    @test InvestNowToSharesight.lookupsharesight("Pathfinder Global Water Fund") == (sharesightfund = "Pathfinder Global Water Fund", instrumentcode = "23773", marketcode = "FundNZ")
    @test InvestNowToSharesight.lookupsharesight("Smartshares - NZ Mid Cap ETF (MDZ)") == (sharesightfund = "Smartshares NZ Mid Cap ETF", instrumentcode = "MDZ", marketcode = "NZX")
    @test InvestNowToSharesight.lookupsharesight("Fisher Funds International Growth Fund") == (sharesightfund = "Fisher Funds International Growth Fund", instrumentcode = "23557", marketcode = "FundNZ")
    @test InvestNowToSharesight.lookupsharesight("Xanguard International Shares Select Exclusions Index Fund") == (sharesightfund = "NOT FOUND", instrumentcode = "NOT FOUND", marketcode = "NOT FOUND")
end

@testset "SharesightTransaction constructor from InvestNowTransaction" begin
    tx = InvestNowTransaction(Date("2021-07-26"), 1517.82, "Buy 123 Something at 12.34", 987654, "Buy", "Something", 12.34, 123, "NZD", 1.23)

    sstx = SharesightTransaction(tx)
    @test sstx.tradedate == Date("2021-07-26")
    @test sstx.transactiontype == "BUY"
    @test sstx.marketcode === "NOT FOUND"
    @test sstx.instrumentcode === "NOT FOUND"
    @test sstx.comment == "Buy 123 Something at 12.34"
    @test sstx.quantity == 123.0
    @test sstx.price == 12.34
    @test sstx.amount == 1517.82
    @test sstx.currency === "NZD"
    @test sstx.uniqueId === "InvestNow:2021-07-26:1517.82:987654:Buy 123 Something at 12.34"
end

@testset "SharesightTransaction constructor from InvestNowTransaction - with `nothing`` `type`" begin
    tx = InvestNowTransaction(Date("2021-07-26"), 1517.82, "Buy 123 Something at 12.34", 987654, nothing, "Something", 12.34, 123, "NZD", 1.23)

    sstx = SharesightTransaction(tx)
    @test sstx.tradedate == Date("2021-07-26")
    @test sstx.transactiontype === nothing
    @test sstx.marketcode === "NOT FOUND"
    @test sstx.instrumentcode === "NOT FOUND"
    @test sstx.comment == "Buy 123 Something at 12.34"
    @test sstx.quantity == 123.0
    @test sstx.price == 12.34
    @test sstx.amount == 1517.82
    @test sstx.currency === "NZD"
    @test sstx.uniqueId === "InvestNow:2021-07-26:1517.82:987654:Buy 123 Something at 12.34"
end

@testset "SharesightTransaction constructor from InvestNowTransaction with lookup" begin
    tx = InvestNowTransaction(Date("2021-07-26"), 3125.96, "Buy 340  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194", 133597, "Buy", "Smartshares - Australian Mid Cap Fund (NS) (MZY)", 9.194, 340, nothing, nothing)

    sstx = SharesightTransaction(tx)
    @test sstx.tradedate == Date("2021-07-26")
    @test sstx.transactiontype == "BUY"
    @test sstx.marketcode === "NZX"
    @test sstx.instrumentcode === "MZY"
    @test sstx.comment == "Buy 340  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194"
    @test sstx.quantity == 340.0
    @test sstx.price == 9.194
    @test sstx.amount == 3125.96
    @test sstx.currency === nothing
    @test sstx.uniqueId === "InvestNow:2021-07-26:3125.96:133597:Buy 340  Smartshares - Australian Mid Cap Fund (NS) (MZY) at 9.194"
end

@testset "Check that input files are sent to the right place" begin
    @test_throws Exception parsefiletosharesightcsv("./input/test.abc", "output.csv")
end

@testset "Create dataframe of InvestNow transactions" begin
   
    json = """
        [
            {
                "date": "2021-12-01T00:00:00+13:00",
                "description": "Reinvest 84.5411 PMG Generation Fund at 1.09, net distribution 92.16 NZD",
                "amount": 92.160000,
                "securityId": 150871
            },
            {
                "date": "2021-12-06T00:00:00+13:00",
                "description": "Realised Currency Loss, Sell 11,202.3033  Vanguard International Shares Select Exclusions Index Fund at 1.7334",
                "amount": -641.144500,
                "securityId": 133276
            },
            {
                "date": "2021-12-08T00:00:00+13:00",
                "description": "Buy 604  Smartshares - NZ Property ETF (NPF) at 1.486",
                "amount": 897.540000,
                "securityId": 137500
            },
            {
                "date": "2021-12-08T00:00:00+13:00",
                "description": "Sell 91  Smartshares - Australian Mid Cap ETF (MZY) at 9.612973",
                "amount": 874.780000,
                "securityId": 133597
            }
        ]
        """

    in_txn_df = InvestNow.parsejsontransactions(json)  # Yields a DataFrame with 4 columns

    # Create a new column containing `InvestNowTransaction`s
    in_txn_df.in_txn .= InvestNowTransaction.(in_txn_df.date, in_txn_df.amount, in_txn_df.description, in_txn_df.securityId)

    @test nrow(in_txn_df) == 4
    @test nrow(filter(:in_txn => InvestNow.buysellfilter, in_txn_df)) == 2

end

@testset "Convert a DataFrame of InvestNow transactions to a DataFrame of Sharesight transactions" begin
   
    json = """
        [
            {
                "date": "2021-12-01T00:00:00+13:00",
                "description": "Reinvest 84.5411 PMG Generation Fund at 1.09, net distribution 92.16 NZD",
                "amount": 92.160000,
                "securityId": 150871
            },
            {
                "date": "2021-12-06T00:00:00+13:00",
                "description": "Realised Currency Loss, Sell 11,202.3033  Vanguard International Shares Select Exclusions Index Fund at 1.7334",
                "amount": -641.144500,
                "securityId": 133276
            },
            {
                "date": "2021-12-08T00:00:00+13:00",
                "description": "Buy 604  Smartshares - NZ Property ETF (NPF) at 1.486",
                "amount": 897.540000,
                "securityId": 137500
            },
            {
                "date": "2021-12-08T00:00:00+13:00",
                "description": "Sell 91  Smartshares - Australian Mid Cap ETF (MZY) at 9.612973",
                "amount": 874.780000,
                "securityId": 133597
            }
        ]
        """

    in_txn_df = InvestNow.parsejsontransactions(json)  # Yields a DataFrame with 4 columns

    # Create a new column containing `InvestNowTransaction`s
    in_txn_df.in_txn .= InvestNowTransaction.(in_txn_df.date, in_txn_df.amount, in_txn_df.description, in_txn_df.securityId)

    ss_txn_df = DataFrame(SharesightTransaction.(in_txn_df.in_txn))

    @test nrow(in_txn_df) == 4
    @test nrow(ss_txn_df) == 4
end

txn_json_1 = """
[
    {
        "date": "2021-12-01T00:00:00+13:00",
        "description": "Reinvest 84.5411 PMG Generation Fund at 1.09, net distribution 92.16 NZD",
        "amount": 92.160000,
        "securityId": 150871
    },
    {
        "date": "2021-12-06T00:00:00+13:00",
        "description": "Realised Currency Loss, Sell 11,202.3033  Vanguard International Shares Select Exclusions Index Fund at 1.7334",
        "amount": -641.144500,
        "securityId": 133276
    },
    {
        "date": "2021-12-08T00:00:00+13:00",
        "description": "Buy 604  Smartshares - NZ Property ETF (NPF) at 1.486",
        "amount": 897.540000,
        "securityId": 137500
    },
    {
        "date": "2021-12-08T00:00:00+13:00",
        "description": "Sell 91  Smartshares - Australian Mid Cap ETF (MZY) at 9.612973",
        "amount": 874.780000,
        "securityId": 133597
    }
]
"""

txn_json_2 = """
[
    {
        "date": "2021-12-01T00:00:00+13:00",
        "description": "Reinvest 84.5411 PMG Generation Fund at 1.09, net distribution 92.16 NZD",
        "amount": 92.160000,
        "securityId": 150871
    },
    {
        "date": "2021-12-06T00:00:00+13:00",
        "description": "Realised Currency Loss, Sell 11,202.3033  Vanguard International Shares Select Exclusions Index Fund at 1.7334",
        "amount": -641.144500,
        "securityId": 133276
    },
    {
        "date": "2021-12-09T00:00:00+13:00",
        "description": "Buy 605  Smartshares - NZ Property ETF (NPF) at 1.486",
        "amount": 897.540000,
        "securityId": 137500
    },
    {
        "date": "2021-12-09T00:00:00+13:00",
        "description": "Sell 92  Smartshares - Australian Mid Cap ETF (MZY) at 9.612973",
        "amount": 874.780000,
        "securityId": 133597
    }
]
"""

@testset "Merge InvestNow transactions with Sharesight transactions to find no differences" begin
   
    in_txn_df = InvestNow.parsejsontransactions(txn_json_1)  # Yields a DataFrame with 4 columns

    # Create a new column containing `InvestNowTransaction`s
    in_txn_df.in_txn .= InvestNowTransaction.(in_txn_df.date, in_txn_df.amount, in_txn_df.description, in_txn_df.securityId)

    # Create a new column containing `SharesightTransaction`s derived from the `InvestNowTransaction`s
    in_txn_df.ss_txn .= SharesightTransaction.(in_txn_df.in_txn)

    for in_txn in in_txn_df.in_txn
        sharesight = InvestNowToSharesight.lookupsharesight(in_txn.security)
        @test sharesight.marketcode != "NOT FOUND"
        @test sharesight.instrumentcode != "NOT FOUND"
    end

    ss_txn_df = DataFrame(ss_txn=SharesightTransaction.(in_txn_df.in_txn))

    for ss_txn in ss_txn_df.ss_txn
        @test ss_txn.marketcode != "NOT FOUND"
        @test ss_txn.instrumentcode != "NOT FOUND"
    end

    #@test ss_txn_df.ss_txn, 1) == 4


    unrecorded_trades_df_all = antijoin(in_txn_df, ss_txn_df, on=[:ss_txn => :ss_txn])

    @test nrow(in_txn_df) == 4
    @test nrow(ss_txn_df) == 4
    @test nrow(unrecorded_trades_df_all) == 0

end

@testset "Merge InvestNow transactions with Sharesight transactions to find some differences" begin
   
    in_txn_df = InvestNow.parsejsontransactions(txn_json_1)  # Yields a DataFrame with 4 columns

    # Create a new column containing `InvestNowTransaction`s
    in_txn_df.in_txn .= InvestNowTransaction.(in_txn_df.date, in_txn_df.amount, in_txn_df.description, in_txn_df.securityId)

    # Create a new column containing `SharesightTransaction`s derived from the `InvestNowTransaction`s
    in_txn_df.ss_txn .= SharesightTransaction.(in_txn_df.in_txn)

    ss_txn_df = DataFrame(ss_txn=SharesightTransaction.(in_txn_df.in_txn))

    in_txn_df_2 = InvestNow.parsejsontransactions(txn_json_2)  # Yields a DataFrame with 4 columns

    # Create a new column containing `InvestNowTransaction`s
    in_txn_df_2.in_txn .= InvestNowTransaction.(in_txn_df_2.date, in_txn_df_2.amount, in_txn_df_2.description, in_txn_df_2.securityId)

    # Create a new column containing `SharesightTransaction`s derived from the `InvestNowTransaction`s
    in_txn_df_2.ss_txn .= SharesightTransaction.(in_txn_df_2.in_txn)

    unrecorded_trades_df_all = antijoin(in_txn_df_2, ss_txn_df, on=[:ss_txn => :ss_txn])

    @test nrow(in_txn_df) == 4
    @test nrow(ss_txn_df) == 4
    @test nrow(unrecorded_trades_df_all) == 2

end