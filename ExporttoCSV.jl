#!/usr/local/bin/julia

println("ExporttoCSV.jl\n")
#println("$(length(ARGS)) command line arguments: $ARGS\n")

using Pkg
Pkg.activate(".")

#include("./src/InvestNowExporter.jl")
using InvestNowToSharesight
#import InvestNowExporter

using ArgParse

function parse_commandline()
    s = ArgParseSettings()

    @add_arg_table! s begin
#        "--in", "-i"
        "in"
            help = "Input file containing InvestNow transactions in JSON format"
            arg_type = String
            default = "./input/input.json"
        "--out", "-o"
            help = "The output base file name. Defaults to the input file name with the output file type and extension added (e.g. \"-investnow.csv\" or \"-sharesight.csv\") appended"
            arg_type = String
            #default = "./output/output-sharesight.csv"
        "--format", "-f"
            help = "Output format, either \"sharesight\" or \"investnow\". Default is to generate all formats."
            arg_type = String
            #default = "sharesight"
    end

    return parse_args(s)
end

"Derive a base path and filename for the output file"
function deriveoutputfilename(in)
    return joinpath(splitdir(splitdir(in)[1])[1], ("output"), last(splitpath(splitext(in)[1])))
end

function main()
    parsed_args = parse_commandline()

    outfilename = parsed_args["out"]
    if parsed_args["out"] === nothing
        outfilename = deriveoutputfilename(parsed_args["in"])
    end

    println("Command line arguments:")

    println("Input file:    $(parsed_args["in"])")
    println("Output file:   $(outfilename)-\"outputtype\".csv")
    println("Output format: $(parsed_args["format"])")

    #println("Greeting...")
    #greet()
    #println("\n... have greeted")

    if parsed_args["format"] == "investnow"
        println("Producing a file in InvestNow format")
        parsefiletocsv(parsed_args["in"], "$(outfilename)-investnow.csv") # Transactions in InvestNow format    
        println("Finished!")
    elseif parsed_args["format"] == "sharesight" 
        println("Producing a file in Sharesight format")
        parsefiletosharesightcsv(parsed_args["in"], "$(outfilename)-sharesight.csv") # Transactions in Sharesight format    
        println("Finished!")
    else
        println("Producing a file in InvestNow format")
        parsefiletocsv(parsed_args["in"], "$(outfilename)-investnow.csv") # Transactions in InvestNow format    
        println("Producing a file in Sharesight format")
        parsefiletosharesightcsv(parsed_args["in"], "$(outfilename)-sharesight.csv") # Transactions in Sharesight format    
        println("Finished!")
    end
end

main()
