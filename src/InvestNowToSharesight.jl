module InvestNowToSharesight

using Dates
using JSON
using JSONTables
using Tables
using CSV
using DataFrames
using Chain

using InvestNow
using Sharesight

#export SharesightTransaction
#export InvestNowTransaction

export parsejsonfiletosharesightcsv
export parsejsonfiletocsv
export parsefiletocsv
export parsefiletosharesightcsv

greet() = print("Hello Aotearoa!")

investnowdateformat = dateformat"y-m-d"

defaultlookup = """[
    {
        "investnowfund": "Vanguard International Shares Select Exclusions Index Fund",
        "sharesightfund": "Vanguard International Shares Select Exclusions Index Fund",
        "sharesightfundcode": "VAN1579AU",
        "sharesightmarket": "FundAU"
    }]"""

currpwd = pwd()
println("InvestNowExporter: Running in directory $currpwd")

if length(findall("InvestNowExporter.jl/test", currpwd)) == 1
    cd("..") # Run in the root folder of the project
end

lookup = nothing

function getLookup()
    global lookup
    if isnothing(lookup)

        lookupfile = "./input/investnow_to_sharesight.json"
        if !isfile(lookupfile)
            lookupfile = "../input/investnow_to_sharesight.json"
        end    
        if isfile(lookupfile)
            @info "Using lookup from $(lookupfile)"
            println("Using lookup from $(lookupfile)")
            lookup = JSON.parse(open(f -> read(f, String), lookupfile))
        else
            @info "Using default lookup"
            lookup = JSON.parse(defaultlookup)
        end
    end
    return lookup
end

#=
Sharesight specifics
=#

function lookupsharesight(investnowsecurity)
    lookup = getLookup()
    found = findall(x -> x["investnowfund"] == investnowsecurity, lookup)
    if length(found) == 0
        return (sharesightfund="NOT FOUND", instrumentcode="NOT FOUND", marketcode="NOT FOUND")
    else
        foundentry = lookup[found[1]]
        return (sharesightfund=foundentry["sharesightfund"], instrumentcode=foundentry["sharesightfundcode"], marketcode=foundentry["sharesightmarket"])
    end
end

"Convert missing values to a proper `String`"
function fixup(svalue)
    svalue === missing ? "NOT FOUND" : svalue
end

function Sharesight.SharesightTransaction(txn::InvestNowTransaction)
    #if txn.sharesightmarket === nothing && txn.sharesightfundcode === nothing &&& txn.sharesightfundcode === nothing

    sharesight = lookupsharesight(txn.security)
    Sharesight.SharesightTransaction(
        txn.date,
        txn.type === nothing ? nothing : uppercase(txn.type),
        sharesight.marketcode, 
        sharesight.instrumentcode,
        txn.description,
        txn.units,
        txn.price,
        txn.amount,
        txn.currency,
        txn.uniqueId
    )
end

#function SharesightTransaction(txn::InvestNowTransaction, marketcode::Union{String, Missing}, instrumentcode::Union{String, Missing})
function Sharesight.SharesightTransaction(txn::InvestNowTransaction, marketcode::String, instrumentcode::String)
    #if txn.sharesightmarket === nothing && txn.sharesightfundcode === nothing &&& txn.sharesightfundcode === nothing

    #sharesight = lookupsharesight(txn.security)
    Sharesight.SharesightTransaction(
        txn.date,
        txn.type === nothing ? nothing : uppercase(txn.type),
        marketcode, 
        instrumentcode,
        txn.description,
        txn.units,
        txn.price,
        txn.amount,
        txn.currency,
        txn.uniqueId
    )
end

function writetxncsv(txns::Vector{SharesightTransaction}, outfile::String)
    CSV.write(outfile, txns; transform=(col, val) -> something(val, missing), header=["Trade Date", "Transaction Type", "Market Code", "Instrument Code", "Comments", "Quantity", "Price", "Amount", "Currency", "Unique Id"])
end

function parsefiletosharesightcsv(infile::String, outfile::String)
    if endswith(infile, ".csv")
        println("CSV file detected")
        parsecsvfiletosharesightcsv(infile, outfile)
    elseif endswith(infile, ".json")
        println("JSON file detected")
        parsejsonfiletosharesightcsv(infile, outfile)
    else
        error("Not a CSV or JSON input file")
    end
end

function parsejsonfiletosharesightcsv(infile::String, outfile::String)
    println("Processing JSON file")
    txframe = open(jsontable, infile) |> DataFrame

    dftosharesightcsv(txframe, outfile)
end

function parsecsvfiletosharesightcsv(infile::String, outfile::String)
    println("Processing CSV file")
    txframe = CSV.File(infile) |> DataFrame

    dftosharesightcsv(txframe, outfile)
end

function dftosharesightcsv(txframe::DataFrame, outfile::String)
    # Strip out just the date part (ignore time)
    txframe.justdate = Date.(SubString.(txframe.date, 1, 10))

    # Create InvestNowTransactions
    txframe.investnowtx = InvestNowTransaction.(txframe.justdate, txframe.amount, txframe.description, txframe.securityId);

    # Pull out the name of the InvestNow security to make it available below for joining to lookup
    txframe.investnowfund = InvestNow.security.(txframe.investnowtx) #works, but requires function to be created

    if isfile("./input/investnow_to_sharesight.json")
        sharesightlookup = open(jsontable, "./input/investnow_to_sharesight.json") |> DataFrame
    else
        sharesightlookup = jsontable(defaultlookup) |> DataFrame
    end

    # Join to the lookup table to get the Sharesight codes
    leftjoin!(txframe, sharesightlookup, on=:investnowfund)

    # Fix up values that didn't get a match
    txframe.sharesightmarket = fixup.(txframe.sharesightmarket)
    txframe.sharesightfundcode = fixup.(txframe.sharesightfundcode)

    #txframe.sharesighttx = SharesightTransaction.(txframe.investnowtx)

    txframe.sharesighttx = SharesightTransaction.(txframe.investnowtx, txframe.sharesightmarket, txframe.sharesightfundcode)
    @chain txframe.sharesighttx begin
        filter(Sharesight.buysellfilter, _)
        sort
        writetxncsv(_, outfile)
    end
    #writetxncsv(sort(txframe.sharesighttx), outfile)
end

end # module