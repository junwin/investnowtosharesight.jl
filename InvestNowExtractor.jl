#!/bin/env -S julia --project=.
#   Pull information from the InvestNow web site
#   ≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
# 
#   This notebook pulls information from the InvestNow web site.
# 
#   It uses a user's credentials and needs interaction to enter the 2nd factor
#   authentication passcode sent via SMS.

@info pwd()
if splitdir(pwd())[2] == "notebook"
    cd("..") # Switch to the project root folder
end
println("Running from: $(pwd())")

# Dependencies
#include("../src/InvestNowExtractor.jl")
#using .InvestNowExtractor
using InvestNow
using InvestNowToSharesight  # For exporting to Sharesight format

using HTTP
using JSON
using JSONTables
using DataFrames
using CSV
using Dates

# Set up the secrets to login

using ArgParse

function parse_commandline()
    s = ArgParseSettings(description="""Specify the credentials for accessing InvestNow. 
    No arguments are mandatory. 
    Each of username, password and managerId are taken from the following sources, listed in decreasing preference: command line arguments, environment variable, configuration file.

    
    If no arguments are secified and the environemnt variables are not all specified then the default configuration file "./config/secrets.jl" will be used.

    The configuration file should be executable julia code and should set the variables username, password and managerId.
    """)

# If no arguments are specified the following environment variables will be used (if they all exist): INVESTNOW_USERNAME, INVESTNOW_PASSWORD and INVESTNOW_MANAGERID.

    @add_arg_table! s begin
        "--outdir", "-o"
            help = "The directory used for the output files."
            arg_type = String
            default = "./output/"
        "--secretsfile", "-s"
            help = "The file containing the credentials for logging in to InvestNow"
            default = "./config/secrets.jl"
        "--username", "-u"
            help = "InvestNow user name. Overrides the INVESTNOW_USERNAME environment variable if it has been set."
        "--password", "-p"
            help = "InvestNow password. Overrides the INVESTNOW_PASSWORD environment variable if it has been set."
        "--managerId", "-m"
            help = "InvestNow manager ID. Typically a 4 digit number. Overrides the INVESTNOW_MANAGERID environment variable if it has been set."
            arg_type = Int
        "--exitbeforeextract"
            help = "specify this to check that the arguments have been set but don't actually call InvestNow"
            action = :store_true
    end

    return parse_args(s)
end

#println(pwd())

parsed_args = parse_commandline()

@debug "Command line arguments: $(parsed_args)"

#NOTE: To enable debug level log output, set the environment variable JULIA_DEBUG=Extractor

username = nothing
password = nothing
managerId = nothing

# Start with configuration files:
if parsed_args["secretsfile"]  !== nothing
    # Non-default configuration file
    @debug "Using secrets file: $(parsed_args["secretsfile"])"

    if isfile(parsed_args["secretsfile"])
        include(parsed_args["secretsfile"])
        credentials = InvestNowCredentials(username, password, managerId)
    else
        @info("The secrets (config) file does not exist and has not been used: $(parsed_args["secretsfile"])")
    end
end


# Override with command line arguments or environment variables
if parsed_args["username"] !== nothing
    @debug "username from command line"
    username = parsed_args["username"]
elseif haskey(ENV, "INVESTNOW_USERNAME")
    @debug "username from environment variable"
    username = ENV["INVESTNOW_USERNAME"]
end

if parsed_args["password"] !== nothing
    @debug "password from command line"
    password = parsed_args["password"]
elseif haskey(ENV, "INVESTNOW_PASSWORD")
    @debug "password from environment variable"
    password = ENV["INVESTNOW_PASSWORD"]
end

if parsed_args["managerId"] !== nothing
    @debug "managerId from command line"
    managerId = parsed_args["managerId"]
elseif haskey(ENV, "INVESTNOW_MANAGERID")
    @debug "managerId from environment variable"
    managerId = parse(Int64, ENV["INVESTNOW_MANAGERID"])
end

if username !== nothing && password !== nothing && managerId !== nothing 
    credentials = InvestNowCredentials(username, password, managerId)
    println("Using username $(credentials.username) with managerId of $(credentials.managerId)")
else
    println("Credentials have not been provided - exiting!")
    if username === nothing
        println("username has not been provided.")
    end
    if password === nothing
        println("password has not been provided.")
    end
    if managerId === nothing
        println("managerId has not been provided.")
    end
    exit()
end

if parsed_args["exitbeforeextract"]
    println("Exiting before running the extract!")
    exit()
end

#   Login
#   ≡≡≡≡≡≡≡
# 
#   The login is in two steps:
# 
#     1. Initiate two-factor login - this prompts a passcode to be sent to
#        the relevant mobile number
# 
#     2. Set up OAuth credentials using the pascode
# 
#   Step 1 - Initial login request
#   ================================
# 
#   This will look like it fails (i.e. HTTP 400 error) but will trigger a text
#   message containing the pass code

#triggerpasscode(managerId, username, password)
triggerpasscode(credentials)

#   Step 2 - Login with passcode
#   ==============================
# 
#   Try again using the correct passcode

# Wait for the SMS - replace the following value
passcode = ""
while true
    println("\nEnter the passcode (6 digits), or type exit to quit")
    global passcode = readline()
    if occursin(r"^\d{6}$", passcode)
        break
    end
    if uppercase(passcode) == "EXIT"
        exit()
    end
end
# Ensure the passcode is a string of 6 digits

#==
@assert(typeof(passcode) == String, "Passcode starts off as a String")
@assert(length(passcode) == 6, "Passcode should be 6 digits")
passcodenum = parse(Int, passcode)
@assert(typeof(passcodenum) == Int, "Passcode is an Integer")
==#

@info "Using passcode: $passcode"

oauthinfo = login(credentials, passcode)

#   Refresh token
#   ===============
# 
#   https://www.oauth.com/oauth2-servers/making-authenticated-requests/refreshing-an-access-token/
# 
#   The refresh of the token is built in to the data requests below. The
#   following request will demonstrate that the OAuth session is active and that
#   the refresh process works, but won't be needed in normal use.

# oauthinfo = refreshauth(oauthinfo, credentials)

#   Pages
#   =======
# 
#     •  Portfolio: "https://webapi.adminis.co.nz/api/portfolio"
# 
#     •  Trial Balance:
#        "https://webapi.adminis.co.nz/api/portfolio/12345/trialBalance" -
#        uses the portfolio id

#   Get Portfolio information
#   ===========================
# 
#   I.e. Information about each portfolio accessible under this login
# 
#   A DataFrame is returned with a row for each porfolio.

portfoliosdf, oauthinfo = getportfolios(oauthinfo, credentials)
#println()
@info("Portfolio details:")
@info(select(portfoliosdf, ["portfolioId", "entityId", "entityName"]))

#names(portfoliosdf)

#   Get portfolio information
#   ===========================

# For testing, write the raw output

#==
trialBalanceString, oauthinfo = getstringfromapi("https://webapi.adminis.co.nz/api/portfolio/58428/trialBalance", oauthinfo, credentials)
open("./output/trialBalance_58428.json", "w") do f
    write(f, trialBalanceString)
end
==#

#==

portfoliodetails = []

# `portfoliosdf.id`` is a `Vector` of the portfolio `id`s
for portfolio in portfoliosdf.id
    global oauthinfo
    data, oauthinfo = getparseddatafromapi("https://webapi.adminis.co.nz/api/portfolio/$(portfolio)/trialBalance", oauthinfo, credentials)
    append!(portfoliodetails, data)
    # portfolioDict[portfolio] = data
end
@info("There are $(length(portfoliodetails)) portfolios")
#println(length(portfoliodetails))

==#

#   Journal Types
#   ===============
# 
#   The journal account numbers are used to extract the activities.
# 
#   The journal type with an accountType of 30 and accountName of "Capital Issued" 
#   is the one for investment transactions.

journaltypesdf  = DataFrame(code=Int[], desc=String[], accountType=Int[], accountName=String[], accountTypeDesc=String[], portfolioId=Int[])
for portfolioId in portfoliosdf[(portfoliosdf.active .== true), :].portfolioId
    global df
    global oauthinfo
    df, oauthinfo = getdataframefromapi("https://webapi.adminis.co.nz/api/portfolio/$(portfolioId)/activity/journaltypes", oauthinfo, credentials)
    df.portfolioId .= portfolioId

    select!(df, Not(:accountNumber)) # Not interested in the accountNumber (it's missing anyway)

    append!(journaltypesdf, df)
end
@info("Journal Types:\n $journaltypesdf")

# Find the journal accounts tha contain investment activity
capissuedaccountdf = journaltypesdf[(journaltypesdf.accountType .== 30) .& (journaltypesdf.accountName .== "Capital Issued"), :][!, ["code", "portfolioId"]]

#capissuedaccount = journaltypesdf[(journaltypesdf.accountType .== 30) .& (journaltypesdf.accountName .== "Capital Issued"), :][1, 1]

# Add the activity journal account number to the dataframe
leftjoin!(portfoliosdf, capissuedaccountdf, on=:portfolioId)

@info("The investment activity journal accounts for each portfolio are:\n$(select(portfoliosdf, ["portfolioId", "code"]))")

#   Activity
#   ==========

# Get last day of the month
today = Dates.format(Dates.now(), "YYYY-mm-dd")
lastMonthEnd = Date(Dates.lastdayofmonth(Dates.now() - Dates.Month(1)))
thisMonthEnd = Date(Dates.lastdayofmonth(Dates.now()))
nextMonthEnd = Date(Dates.lastdayofmonth(Dates.now() + Dates.Month(1)))

@info("\nlastMonthEnd: $lastMonthEnd\nthisMonthEnd: $thisMonthEnd\nnextMonthEnd: $nextMonthEnd\n")

periodType = InvestNow.YEAR

#   PeriodType 0 = Day 1 = Week 2 = Month 3 = Quarter 4 = Six Months 5 = Year
# 
#   Joint: &show=investments Show all my investment transactions
#   https://webapi.adminis.co.nz/api/portfolio/11111/activity?accountId=1901607&endDate=Thu+Mar+31+2022&periodType=5&show=investments
#   &show=cash Show all my cash transactions
#   https://webapi.adminis.co.nz/api/portfolio/11111/activity?accountId=1901607&endDate=Thu+Mar+31+2022&periodType=5&show=cash
#   &show=transactions Show all my Deposits
#   https://webapi.adminis.co.nz/api/portfolio/11111/activity?accountId=1901607&endDate=Thu+Mar+31+2022&periodType=5&show=transactions
#   &show=transactions Show all my Distributions
#   https://webapi.adminis.co.nz/api/portfolio/11111/activity?accountId=1901610&endDate=Thu+Mar+31+2022&periodType=5&show=transactions
#   &show=transactions Show all my Dividends
#   https://webapi.adminis.co.nz/api/portfolio/11111/activity?accountId=1901609&endDate=Thu+Mar+31+2022&periodType=5&show=transactions
#   &show=transactions Show all my Tax Withheld, Foreign Tax Credit
#   https://webapi.adminis.co.nz/api/portfolio/11111/activity?accountId=1901648&endDate=Thu+Mar+31+2022&periodType=5&show=transactions
#   &show=transactions Show all my Withdrawals Not an option!?!
# 
#   John: &show=investments Show all my investment transactions
#   https://webapi.adminis.co.nz/api/portfolio/22222/activity?accountId=146432&endDate=Thu+Mar+31+2022&periodType=2&show=investments
#   &show=cash Show all my cash transactions
#   https://webapi.adminis.co.nz/api/portfolio/22222/activity?accountId=146432&endDate=Thu+Mar+31+2022&periodType=2&show=cash
#   &show=transactions Show all my Deposits
#   https://webapi.adminis.co.nz/api/portfolio/22222/activity?accountId=146432&endDate=Thu+Mar+31+2022&periodType=2&show=transactions
#   &show=transactions Show all my Distributions
#   https://webapi.adminis.co.nz/api/portfolio/22222/activity?accountId=146438&endDate=Thu+Mar+31+2022&periodType=2&show=transactions
#   &show=transactions Show all my Dividends
#   https://webapi.adminis.co.nz/api/portfolio/22222/activity?accountId=146437&endDate=Thu+Mar+31+2022&periodType=2&show=transactions
#   &show=transactions Show all my Tax Withheld, Foreign Tax Credit
#   https://webapi.adminis.co.nz/api/portfolio/22222/activity?accountId=146474&endDate=Thu+Mar+31+2022&periodType=2&show=transactions
#   &show=transactions Show all my Withdrawals
#   https://webapi.adminis.co.nz/api/portfolio/22222/activity?accountId=146433&endDate=Thu+Mar+31+2022&periodType=2&show=transactions

#   curl
#   'https://webapi.adminis.co.nz/api/portfolio/11111/activity?accountId=1901607&endDate=Thu+Mar+31+2022&periodType=5&show=transactions'
#   -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:98.0)
#   Gecko/20100101 Firefox/98.0' -H 'Accept: application/json, text/plain, /' -H
#   'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate, br' -H
#   'Authorization: Bearer
#   eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTUxMiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE2NDgzNTc3NzEsImV4cCI6MTY0ODM1ODA3MSwiaXNzIjoiaHR0cHM6Ly9sb2dpbmFwaS5hZG1pbmlzLmNvLm56IiwiYXVkIjpbImh0dHBzOi8vbG9naW5hcGkuYWRtaW5pcy5jby5uei9yZXNvdXJjZXMiLCJ3ZWJfY2xpZW50Il0sImNsaWVudF9pZCI6IndlYl9jbGllbnQiLCJzdWIiOiJqb2huLnVud2luQGZpbmdlcnRpcC5jby5ueiIsImF1dGhfdGltZSI6MTY0ODM1NzQxMSwiaWRwIjoibG9jYWwiLCJ1bmlxdWVfbmFtZSI6ImpvaG4udW53aW5AZmluZ2VydGlwLmNvLm56IiwiZ2l2ZW5fbmFtZSI6IkpvaG4gVW53aW4iLCJwcmltYXJ5Z3JvdXBzaWQiOiI0NTQyIiwicHJpbWFyeXNpZCI6IjI1NjQ3IiwiZ3JvdXBzaWQiOiI0NTQyIiwibWFuYWdlck5hbWUiOiJJbnZlc3ROb3ciLCJtb2R1bGUtc2NvcGUiOiI2NjEiLCJyb2xlIjpbIjI1NjQ4LDEwLDE0LDExNCwxLE0sMjAiLCIyMDA5MjIsMTAsMTQsMTE0LDEsTSwyMCJdLCJjbGllbnQtc2NvcGUiOiJ3ZWJfY2xpZW50IiwidXNlci1zY29wZSI6WyJBdXRob3JpemVyIiwiT25saW5lIEFjY2VzcyJdLCJzY29wZSI6WyJ3ZWJfY2xpZW50Iiwib2ZmbGluZV9hY2Nlc3MiXSwiYW1yIjpbInBhc3N3b3JkIl19.lQFeSRIzQr32UYULrEkNGKeNd8cY5d7X93d4bix8GxQhxwtOMou-BcgC-P8zRYGbRJajNcRZltb2SuOiNuo3sA'
#   -H 'Origin: https://secure.investnow.co.nz' -H 'Connection: keep-alive' -H
#   'Sec-Fetch-Dest: empty' -H 'Sec-Fetch-Mode: cors' -H 'Sec-Fetch-Site:
#   cross-site' -H 'Sec-GPC: 1'

#==
#periodType = 0 # Day
#periodType = 1 # Week
periodType = 2 # Month
#periodType = 3 # quarter
#periodType = 4 # 6 months
#periodType = 5 # Year

@enum PeriodType DAY=0 WEEK MONTH QUARTER SIXMONTHS YEAR
==#

# Now inherited from InvestNowExtractor.jl

#   Get Transaction Activity
#   ==========================
# 
#   Requires several parameters:
# 
#     •  Portfolio Id - portflios should have been extracted above and the
#        appropriate one identified (portfolioindex)
# 
#     •  The is of the "Capital Issued" journal account - it should have
#        been extracted above (capissuedaccount)
# 
#     •  the date of the last day in the period of interest
# 
#     •  the period of interest, using the InvestNowExtractor PeriodType
#        enumerator - e.g. InvestNowExtractor.DAY,
#        InvestNowExtractor.MONTH, etc.

activitydf  = DataFrame(date=String[], description=String[], amount=Union{Float64, Int64}[], securityId=Int[], portfolioId=Int[], transaction=InvestNowTransaction[])
for p in eachrow(portfoliosdf[!, [:portfolioId, :code]])
    #df, oauthinfo = getdataframefromapi("https://webapi.adminis.co.nz/api/portfolio/$(id)/activity/journaltypes", oauthinfo, credentials)
    global df 
    global oauthinfo
    df, oauthinfo = getinvestmentactivity(p.portfolioId, p.code, thisMonthEnd, periodType, oauthinfo, credentials)
    
    df.portfolioId .= p.portfolioId # Add the portfolio id

    #df.accountNumber = missingtonothing.(df.accountNumber) # accountNumber values are all missing

    #select!(df, Not(:accountNumber)) # Not interested in the accountNumber (it's missing anyway)

    append!(activitydf, df)
end
#journaltypesdf[!, ["id", "code", "desc", "accountType", "accountName"]]
#println(activitydf)

for p in eachrow(portfoliosdf)
    @info("Writing CSV for portfiolio id: $(p.portfolioId), date: $thisMonthEnd, period: $periodType")
    thisportfoliodf = filter(t -> t.portfolioId == p.portfolioId, activitydf)
    csvfilename = "./output/investmenttransactions-$(p.portfolioId)-$today-to-$thisMonthEnd-$periodType-sharesight.csv"
    @info("Writing $csvfilename")
    InvestNowToSharesight.dftosharesightcsv(thisportfoliodf[:, :], csvfilename)
end