# Running the InvestNowToSharesight `InvestNowExtractor.jl` in a container

The InvestNow Extractor can run in a container. It has been tested running from [Podman](https://podman.io/) but ought to be compatible with [Docker](https://www.docker.com/) too. You can very probably just substitute `docker` for `podman` in the commands listed below.

There are two methods provided:

   1. Extract to a CSV file (suitable for importing to Sharesight)
   1. Run a Pluto notebook that can update Sharesight directly using the officail Sharesight API

## Scripts

Scripts are available in the `podman` folder:

| Script name | Function |
| ----- | -----| 
| `run_extract_in_container.sh` | Extracts to a CSV file |
| `run_pluto_in_container.sh` | Run the Pluto notebook |
| `run_shell_in_container.sh` | Start a `bash` shell in the container. Useful for exploring the contents and functionality of the container image. |

## Examples

A full example using a configuration file to specify credentials:

```sh
podman run -ti --rm --mount type=bind,src=./output,dst=/workspace/investnowtosharesight.jl/output --mount type=bind,src=./config,dst=/workspace/investnowtosharesight.jl/config docker.io/fingertip/investnowtosharesight julia --project InvestNowExtractor.jl -s=./config/overiddensecrets.jl
```

Using environment variable to pass credentials (not for the Pluto notebook):

```sh
export INVESTNOW_USERNAME=fred@fred.com
export INVESTNOW_PASSWORD=B1g5ecret
export INVESTNOW_MANAGERID=1234
podman run -ti --rm -e INVESTNOW_USERNAME -e INVESTNOW_PASSWORD -e INVESTNOW_MANAGERID --mount type=bind,src=./output,dst=/workspace/investnowtosharesight.jl/output --mount type=bind,src=./config,dst=/workspace/investnowtosharesight.jl/config docker.io/fingertip/investnowtosharesight julia --project InvestNowExtractor.jl
```

Or without creating the variables beforehand:

```sh
podman run -ti --rm -e INVESTNOW_USERNAME=fred@fred.com -e INVESTNOW_PASSWORD=B1g5ecret -e INVESTNOW_MANAGERID=1234 --mount type=bind,src=./output,dst=/workspace/investnowtosharesight.jl/output --mount type=bind,src=./config,dst=/workspace/investnowtosharesight.jl/config docker.io/fingertip/investnowtosharesight julia --project InvestNowExtractor.jl
```

Passing credentials on the command line:

```sh
podman run -ti --rm --mount type=bind,src=./output,dst=/workspace/investnowtosharesight.jl/output --mount type=bind,src=./config,dst=/workspace/investnowtosharesight.jl/config docker.io/fingertip/investnowtosharesight julia --project InvestNowExtractor.jl -u fred@fred.com -p 5ecr3t -m 1234
```

Or, with the arguments fully specified:

```sh
podman run -ti --rm --mount type=bind,src=./output,dst=/workspace/investnowtosharesight.jl/output --mount type=bind,src=./config,dst=/workspace/investnowtosharesight.jl/config docker.io/fingertip/investnowtosharesight julia --project InvestNowExtractor.jl -username fred@fred.com -password 5ecr3t -managerId 1234
```

## Credentials

The container images don't contain credentials. You must provide credentials for:

  - InvestNow
  - Sharesight (only needed for the Pluto notebook)

### Configuration file

A configuration file will be used at `./config/secrets.jl`.

## Mapping folders to external locations

The output folder ought to be mapped outside the running container (otherwise it won't exist, will be unwritable or you will lose the content when the container shuts down).

Map using the `mount` option. E.g.

```sh
podman run -ti --rm --mount type=bind,src=/home/john/Code/podman/investnow-extractor-container/output,dst=/workspace/investnowtosharesight.jl/output docker.io/fingertip/investnowtosharesight julia --project=/workspace/investnowtosharesight.jl /workspace/investnowtosharesight.jl/InvestNowExtractor.jl
```

The location of the configuration file can be mounted externally:

```sh
--mount type=bind,src=./config,dst=/workspace/investnowtosharesight.jl/config
```

## Testing the configuration and credentials - `exitbeforeextract`

An option to parse the configuration but not actually call InvestNow can be invoked with the `--exitbeforeextract` command line option.

E.g. 

```sh
podman run -ti --rm --mount type=bind,src=./output,dst=/workspace/investnowtosharesight.jl/output --mount type=bind,src=./config,dst=/workspace/investnowtosharesight.jl/config docker.io/fingertip/investnowtosharesight julia --project InvestNowExtractor.jl --exitbeforeextract
```

## Debug output

Debug output will be generated if the environment variable `JULIA_DEBUG` is set to `InvestNowExtractor`.

```sh
podman run -e JULIA_DEBUG=InvestNowExtractor [etc...]
```

