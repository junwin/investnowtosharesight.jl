# !/bin/bash

# Run the extract from the container, using configuration in the local context

podman run -ti --rm --mount type=bind,src=../output,dst=/workspace/investnowtosharesight.jl/output \
    --mount type=bind,src=../config,dst=/workspace/investnowtosharesight.jl/config \
    -p 8090:8090 docker.io/fingertip/investnowtosharesight \
    julia --project=. InvestNowExtractor.jl