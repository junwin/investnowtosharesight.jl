# !/bin/bash

# Build the container image
podman build --no-cache -t docker.io/fingertip/investnowtosharesight .

# Push to Docker Hub
podman push fingertip/investnowtosharesight:latest