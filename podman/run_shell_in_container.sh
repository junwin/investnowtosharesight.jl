# !/bin/bash

# Open a shell in the container
podman run -ti --rm --mount type=bind,src=../output,dst=/workspace/investnowtosharesight.jl/output --mount type=bind,src=../config,dst=/workspace/investnowtosharesight.jl/config docker.io/fingertip/investnowtosharesight bash
